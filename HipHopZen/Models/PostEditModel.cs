﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HipHopZen.Models
{
    public class PostEditModel
    {
        public int Id { get; set; }

        [StringLength(200)]
        [Required(ErrorMessage = "The Title field is required")]
        public string Title { get; set; }

        [Required(ErrorMessage = "The Description field is required")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public string Rating { get; set; }

        public string Price { get; set; }

        [StringLength(300)]
        public string URL { get; set; }

        [MaxLength]
        [DisplayName("Picture")]
        public byte[] PhotoFile { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ImageMimeType { get; set; }

        [StringLength(300)]
        public string VideoURL { get; set; }

        [DisplayName("Category")]
        public string CategoryName { get; set; }
    }
}