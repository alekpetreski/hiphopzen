﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace HipHopZen.Models
{
    public class CommentViewModel
    {
        public int Id { get; set; }

        public string Nickname { get; set; }

        public string Text { get; set; }

        public DateTime Date { get; set; }
    }
}