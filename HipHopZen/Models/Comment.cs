﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HipHopZen.Models
{
    public class Comment
    {
        public int Id { get; set; }

        [StringLength(20)]
        [Required(ErrorMessage = "Write a nickname")]
        public string Nickname { get; set; }

        [StringLength(200)]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Write a comment")]
        public string Text { get; set; }

        [DisplayFormat(DataFormatString = "{0:HH:mm dd/MM/yyyy}")]
        public DateTime Date { get; set; }

        public int PostID { get; set; }

        public virtual Post Post{ get; set; }

        public Comment()
        {
            this.Date = DateTime.Now;
        }

        public Comment(string nickname, string text, int postId, Post post)
        {
            this.Nickname = nickname;
            this.Text = text;
            this.Date = DateTime.Now;
            this.PostID = postId;
            this.Post = post;
        }
    }

}