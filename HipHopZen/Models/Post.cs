﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HipHopZen.Models
{
    public class Post
    {
        public int Id { get; set; }

        [StringLength(200)]
        [Required(ErrorMessage = "The Title field is required")]
        public string Title { get; set; }

        [Required(ErrorMessage = "The Description field is required")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }

        public int Votes { get; set; }

        public string Rating { get; set; }

        public string Price { get; set; }

        [StringLength(300)]
        public string URL { get; set; }

        [MaxLength]
        [DisplayName("Picture 1")]
        public byte[] PhotoFile1 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ImageMimeType1 { get; set; }

        [MaxLength]
        [DisplayName("Picture 2")]
        public byte[] PhotoFile2 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ImageMimeType2 { get; set; }

        [MaxLength]
        [DisplayName("Picture 3")]
        public byte[] PhotoFile3 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ImageMimeType3 { get; set; }

        [StringLength(300)]
        public string VideoURL { get; set; }

        public string UserID { get; set; }

        public virtual ApplicationUser User { get; set; }

        public int CategoryID { get; set; }

        [Required(ErrorMessage = "The Category field is required")]
        public virtual Category Category { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public Post() { }

        public Post(string title, string description, int votes, string rating, string price, string url, byte[] photoFile, string imageMimeType, string videoUrl, string userId, int categoryId)
        {
            this.Title = title;
            this.Description = description;
            this.Date = DateTime.Now;
            this.Votes = votes;
            this.Rating = rating;
            this.Price = price;
            this.URL = url;
            this.PhotoFile = photoFile;
            this.ImageMimeType = imageMimeType;
            this.VideoURL = videoUrl;
            this.UserID = userId;
            this.CategoryID = categoryId;
            this.Comments = new List<Comment>();
        }
    }
}