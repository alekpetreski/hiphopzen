﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HipHopZen.Models
{
    public class PostDetailsModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime Date { get; set; }

        public int Votes { get; set; }

        public string Rating { get; set; }

        public string Price { get; set; }

        public string URL { get; set; }

        public byte[] PhotoFile { get; set; }

        public string ImageMimeType { get; set; }

        public string VideoURL { get; set; }

        public string UserName { get; set; }

        public string CategoryName { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
    }
}