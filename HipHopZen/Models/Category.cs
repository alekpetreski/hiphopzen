﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HipHopZen.Models
{
    [Serializable]
    public class Category
    {
        public int Id { get; set; }

        [StringLength(100)]
        [DisplayName("Category")]
        [Required(ErrorMessage = "The Category field is required")]
        public string Name { get; set; }

        public virtual ICollection<Post> Posts { get; set; }

        public Category() { }

        public Category(string name)
        {
            this.Name = name;
            Posts = new List<Post>();
        }
    }
}