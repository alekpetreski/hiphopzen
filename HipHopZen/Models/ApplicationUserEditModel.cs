﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HipHopZen.Models
{
    public class ApplicationUserEditModel
    {
        public string Id { get; set; }

        [StringLength(30)]
        [Required(ErrorMessage = "The Name field is required")]
        public string Name { get; set; }

        [StringLength(30)]
        [Required(ErrorMessage = "The Surname field is required")]
        public string Surname { get; set; }

        [StringLength(30)]
        [Required(ErrorMessage = "The Username field is required")]
        public string UserName { get; set; }
    }
}