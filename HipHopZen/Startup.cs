﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HipHopZen.Startup))]
namespace HipHopZen
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
