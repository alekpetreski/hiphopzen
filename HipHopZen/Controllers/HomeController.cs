﻿using HipHopZen.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HipHopZen.Controllers
{
    [HandleError(View = "Error")]
    public class HomeController : Controller
    {
        ApplicationDbContext context;

        public HomeController()
        {
            context = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            var posts = (from p in context.Posts
                         orderby p.Date descending
                         select new PostDetailsModel()
                         {
                             Id = p.Id,
                             Title = p.Title,
                             Description = p.Description,
                             Date = p.Date,
                             Votes = p.Votes,
                             Rating = p.Rating,
                             Price = p.Price,
                             URL = p.URL,
                             PhotoFile = p.PhotoFile,
                             ImageMimeType = p.ImageMimeType,
                             VideoURL = p.VideoURL,
                             CategoryName = p.Category.Name
                         }).Take(6);
            ViewBag.ShowAll = false;
            return View("Index", posts);
        }

        public ActionResult Show()
        {
            var posts = from p in context.Posts
                        orderby p.Date descending
                        select new PostDetailsModel()
                        {
                            Id = p.Id,
                            Title = p.Title,
                            Description = p.Description,
                            Date = p.Date,
                            Votes = p.Votes,
                            Rating = p.Rating,
                            Price = p.Price,
                            URL = p.URL,
                            PhotoFile = p.PhotoFile,
                            ImageMimeType = p.ImageMimeType,
                            VideoURL = p.VideoURL,
                            CategoryName = p.Category.Name,
                        };
            ViewBag.ShowAll = true;
            return View("Index", posts);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public FileContentResult GetImage(int id)
        {
            Post post = context.Posts.Find(id);
            if (post.PhotoFile != null)
            {
                return File(post.PhotoFile, post.ImageMimeType);
            }
            else
            {
                return null;
            }
        }
    }
}