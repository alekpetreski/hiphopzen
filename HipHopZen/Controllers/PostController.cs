﻿using HipHopZen.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HipHopZen.Controllers
{
    [HandleError(View = "Error")]
    public class PostController : Controller
    {
        private ApplicationDbContext context;

        public PostController()
        {
            context = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Show()
        {
            return View("Show");
        }

        [Authorize]
        public ActionResult PostList()
        {
            var posts = from p in context.Posts
                        orderby p.Date descending
                        select new PostDetailsModel()
                        {
                            Id = p.Id,
                            Title = p.Title,
                            Description = p.Description,
                            Date = p.Date,
                            Votes = p.Votes,
                            Rating = p.Rating,
                            Price = p.Price,
                            URL = p.URL,
                            PhotoFile = p.PhotoFile,
                            ImageMimeType = p.ImageMimeType,
                            VideoURL = p.VideoURL,
                            CategoryName = p.Category.Name,
                        };
            return PartialView("_PostList", posts);
        }

        [HttpGet]
        [Authorize]
        public ActionResult Create()
        {
            List<string> categories = new List<string>();
            foreach (Category c in context.Categories.ToList())
            {
                categories.Add(c.Name);
            }

            ViewBag.Categories = new SelectList(categories);
            return View("Create");
        }

        [HttpPost]
        [Authorize]
        public ActionResult Create(Post model, HttpPostedFileBase image)
        {
            if (model != null && this.ModelState.IsValid)
            {
                Category category = (from c in context.Categories
                                     where c.Name == model.Category.Name
                                     select c).FirstOrDefault();
                if (category != null && image != null)
                {
                    Post post = new Post()
                    {
                        Title = model.Title,
                        Description = model.Description,
                        Date = DateTime.Today,
                        Votes = 0,
                        Rating = model.Rating,
                        Price = model.Price,
                        URL = model.URL,
                        PhotoFile = new byte[image.ContentLength],
                        ImageMimeType = image.ContentType,
                        VideoURL = model.VideoURL,
                        UserID = User.Identity.GetUserId(),
                        CategoryID = category.Id,
                        Category = category,
                        Comments = new List<Comment>()
                    };
                    image.InputStream.Read(post.PhotoFile, 0, image.ContentLength);
                    context.Posts.Add(post);
                    context.SaveChanges();
                }
                return RedirectToAction("Show");
            }
            return View(model);
        }

        [Authorize]
        public ActionResult Delete(int id)
        {
            Post post = context.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            context.Posts.Remove(post);
            context.SaveChanges();
            return RedirectToAction("PostList");
        }


        [HttpGet]
        [Authorize]
        public ActionResult Edit(int id)
        {
            var posts = from p in context.Posts
                        where p.Id == id
                        select new PostEditModel
                        {
                            Id = p.Id,
                            Title = p.Title,
                            Description = p.Description,
                            Rating = p.Rating,
                            Price = p.Price,
                            URL = p.URL,
                            PhotoFile = p.PhotoFile,
                            ImageMimeType = p.ImageMimeType,
                            VideoURL = p.VideoURL,
                            CategoryName = p.Category.Name
                        };
            return View("Edit", posts.FirstOrDefault());
        }

        [HttpPost]
        [Authorize]
        public ActionResult Edit(PostEditModel model, HttpPostedFileBase image)
        {
            if (model != null && this.ModelState.IsValid)
            {
                Post post = context.Posts.Find(model.Id);
                post.Title = model.Title;
                post.Description = model.Description;
                post.Rating = model.Rating;
                post.Price = model.Price;
                post.URL = model.URL;
                post.VideoURL = model.VideoURL;
                if (image != null)
                {
                    post.PhotoFile = new byte[image.ContentLength];
                    post.ImageMimeType = image.ContentType;
                    image.InputStream.Read(post.PhotoFile, 0, image.ContentLength);
                }

                Category category = (from c in context.Categories
                                     where c.Name == model.CategoryName
                                     select c).FirstOrDefault();
                post.CategoryID = category.Id;
                post.Category = category;
                context.SaveChanges();

                return RedirectToAction("Show");
            }
            return View("Edit", model);
        }

        public ActionResult Details(int id)
        {
            var post = from p in context.Posts
                       where p.Id == id
                       select new PostDetailsModel
                       {
                           Id = p.Id,
                           Title = p.Title,
                           Description = p.Description,
                           Date = p.Date,
                           Votes = p.Votes,
                           Rating = p.Rating,
                           Price = p.Price,
                           URL = p.URL,
                           PhotoFile = p.PhotoFile,
                           ImageMimeType = p.ImageMimeType,
                           VideoURL = p.VideoURL,
                           UserName = p.User.Name,
                           CategoryName = p.Category.Name,
                           Comments = p.Comments
                       };
            return View("Details", post.FirstOrDefault());
        }

        public ActionResult AddVote(int id)
        {
            Post post = (from p in context.Posts
                         where p.Id == id
                         select p).FirstOrDefault();
            post.Votes += 1;
            context.SaveChanges();

            return PartialView("_Votes", post);
        }

        public ActionResult SearchByCategory(string category)
        {
            var posts = from p in context.Posts
                        where p.Category.Name == category
                        orderby p.Date descending
                        select new PostDetailsModel
                        {
                            Id = p.Id,
                            Title = p.Title,
                            Description = p.Description,
                            Date = p.Date,
                            Votes = p.Votes,
                            Rating = p.Rating,
                            Price = p.Price,
                            URL = p.URL,
                            PhotoFile = p.PhotoFile,
                            ImageMimeType = p.ImageMimeType,
                            VideoURL = p.VideoURL,
                            UserName = p.User.Name,
                            CategoryName = p.Category.Name,
                            Comments = p.Comments
                        };
            if (string.IsNullOrEmpty(category) || string.IsNullOrWhiteSpace(category))
            {
                ViewBag.Category = null;
                ViewBag.Total = 0;
            }
            else
            {
                ViewBag.Category = category;
                ViewBag.Total = posts.Count();
            }
            return View("SearchByCategory", posts);
        }

        public ActionResult GetCategories()
        {
            List<string> categories = new List<string>();
            foreach (Category c in context.Categories.ToList())
            {
                categories.Add(c.Name);
            }
            return Json(categories, JsonRequestBehavior.AllowGet);
        }

        public FileContentResult GetImage(int id)
        {
            Post post = context.Posts.Find(id);
            if (post.PhotoFile != null)
            {
                return File(post.PhotoFile, post.ImageMimeType);
            }
            else
            {
                return null;
            }
        }
    }
}