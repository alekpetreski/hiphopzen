﻿using HipHopZen.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HipHopZen.Controllers
{
    public class CategoryController : Controller
    {
        ApplicationDbContext context;

        public CategoryController()
        {
            context = new ApplicationDbContext();
        }

        // GET: Category
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Show()
        {
            return View("Show");
        }

        [HttpGet]
        [Authorize]
        public ActionResult Create()
        {
            return View("Create");
        }

        [HttpPost]
        [Authorize]
        public ActionResult Create(Category model)
        {
            Category category = new Category(model.Name);
            context.Categories.Add(category);
            context.SaveChanges();
            return RedirectToAction("Show");
        }

        public ActionResult CategoryList()
        {
            var categories = from c in context.Categories
                             orderby c.Name ascending
                             select c;
            return PartialView("_CategoryList", categories);
        }

        [Authorize]
        public ActionResult Delete(int id)
        {
            Category category = context.Categories.Find(id);
            var posts = from p in context.Posts
                        where p.CategoryID == id
                        select p;
            Category other = (from c in context.Categories
                              where c.Name == "Other"
                              select c).FirstOrDefault();
            foreach (Post p in posts)
            {
                p.CategoryID = other.Id;
                p.Category = other;
            }

            context.Categories.Remove(category);
            context.SaveChanges();
            return RedirectToAction("CategoryList");
        }

        public ActionResult SearchByCategory(string category)
        {
            var posts = from p in context.Posts
                        where p.Category.Name == category
                        orderby p.Date descending
                        select new PostDetailsModel
                        {
                            Id = p.Id,
                            Title = p.Title,
                            Description = p.Description,
                            Date = p.Date,
                            Votes = p.Votes,
                            Rating = p.Rating,
                            Price = p.Price,
                            URL = p.URL,
                            PhotoFile = p.PhotoFile,
                            ImageMimeType = p.ImageMimeType,
                            VideoURL = p.VideoURL,
                            UserName = p.User.Name,
                            CategoryName = p.Category.Name,
                            Comments = p.Comments
                        };
            if (string.IsNullOrEmpty(category) || string.IsNullOrWhiteSpace(category))
            {
                ViewBag.Category = null;
                ViewBag.Total = 0;
            }
            else
            {
                ViewBag.Category = category;
                ViewBag.Total = posts.Count();
            }
            return View("SearchByCategory", posts);
        }
    }
}