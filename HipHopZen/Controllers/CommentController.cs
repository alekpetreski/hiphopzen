﻿using HipHopZen.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HipHopZen.Controllers
{
    public class CommentController : Controller
    {
        ApplicationDbContext context;

        public CommentController()
        {
            context = new ApplicationDbContext();
        }

        // GET: Comment
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public PartialViewResult _Create(int postId)
        {
            Comment comment = new Comment();
            comment.PostID = postId;

            ViewBag.PostId = postId;
            return PartialView("_CreateAComment");
        }

        // GET: Action which return PartialView for displaying the comments of a given Post
        [ChildActionOnly]   //The action cannot be accessed from the browser's address bar
        public PartialViewResult _CommentsForPost(int postId)
        {
            var comments = from c in context.Comments
                           where c.PostID == postId
                           select c;

            ViewBag.PostId = postId;
            return PartialView("_CommentsForPost", comments.ToList());
        }

        //POST: Action which saves and shows a comment when the AJAX comment create tool is used
        [HttpPost]
        public PartialViewResult _CommentsForPost(Comment comment, int postId)
        {
            context.Comments.Add(comment);  //Save the comment in the database
            context.SaveChanges();

            var comments = from c in context.Comments   //Get the updated list of comments
                           where c.PostID == postId
                           select c;

            ViewBag.PostId = postId;
            return PartialView("_ShowComments", comments.ToList());
        }

        [Authorize]
        public ActionResult Delete(int id)
        {
            Comment comment = context.Comments.Find(id);
            if(comment == null)
            {
                return HttpNotFound();
            }
            int postId = comment.PostID;
            context.Comments.Remove(comment);
            context.SaveChanges();

            var comments = from c in context.Comments   //Get the updated list of comments
                           where c.PostID == postId
                           select c;

            ViewBag.PostId = postId;
            return PartialView("_ShowComments", comments.ToList());
        }
    }
}