﻿using HipHopZen.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HipHopZen.Controllers
{
    [Authorize]
    public class ApplicationUserController : Controller
    {
        ApplicationDbContext context;

        public ApplicationUserController()
        {
            context = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Details(string id)
        {
            var user = from u in context.Users
                       where u.Id == id
                       select new ApplicationUserDetailsModel
                       {
                           Id = u.Id,
                           Name = u.Name,
                           Surname = u.Surname,
                           UserName = u.UserName,
                           Email = u.Email
                       };
            return View("Details", user.FirstOrDefault());
        }

        [HttpGet]
        public ActionResult Edit()
        {
            string id = User.Identity.GetUserId();
            var user = from u in context.Users
                       where u.Id == id
                       select new ApplicationUserEditModel
                       {
                           Id = u.Id,
                           Name = u.Name,
                           Surname = u.Surname,
                           UserName = u.UserName
                       };
            return View("Edit", user.FirstOrDefault());
        }

        [HttpPost]
        public ActionResult Edit(ApplicationUserEditModel model)
        {
            if (model != null && this.ModelState.IsValid)
            {
                ApplicationUser user = context.Users.Find(User.Identity.GetUserId());
                user.Name = model.Name;
                user.Surname = model.Surname;
                user.UserName = model.UserName;
                context.SaveChanges();

                ApplicationUserDetailsModel userDetails = new ApplicationUserDetailsModel();
                userDetails.Id = user.Id;
                userDetails.Name = user.Name;
                userDetails.Surname = user.Surname;
                userDetails.UserName = user.UserName;
                userDetails.Email = user.Email;

                return RedirectToAction("Details", userDetails);
            }
            return View("Edit", model);
        }
    }
}