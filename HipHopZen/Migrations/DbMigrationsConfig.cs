namespace HipHopZen.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.IO;
    using System.Linq;
    using System.Web;

    internal sealed class DbMigrationsConfig : DbMigrationsConfiguration<HipHopZen.Models.ApplicationDbContext>
    {
        public DbMigrationsConfig()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "HipHopZen.Models.ApplicationDbContext";
        }

        protected override void Seed(HipHopZen.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            ///<summary>
            ///Create admin
            /// </summary>
            //ApplicationUser adminUser = new ApplicationUser
            //{
            //    Name = "Admin",
            //    Surname = "Admin",
            //    Email = "admin@test.com",
            //    UserName = "admin@test.com"
            //};

            //var userStore = new UserStore<ApplicationUser>(context);
            //var userManager = new UserManager<ApplicationUser>(userStore);
            //userManager.PasswordValidator = new PasswordValidator
            //{
            //    RequiredLength = 4,
            //    RequireNonLetterOrDigit = false,
            //    RequireDigit = true,
            //    RequireLowercase = true,
            //    RequireUppercase = false,
            //};

            //var userCreateResult = userManager.Create(adminUser, "admin123");
            //if (!userCreateResult.Succeeded)
            //{
            //    throw new Exception(string.Join("; ", userCreateResult.Errors));
            //}

            //var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            //var roleCreateResult = roleManager.Create(new IdentityRole(("Admin")));
            //if (!roleCreateResult.Succeeded)
            //{
            //    throw new Exception(string.Join("; ", roleCreateResult.Errors));
            //}

            //var addAdminRoleResult = userManager.AddToRole(adminUser.Id, "Admin");
            //if (!addAdminRoleResult.Succeeded)
            //{
            //    throw new Exception(string.Join("; ", addAdminRoleResult.Errors));

            //}

            ///<summary>
            ///Create categories
            /// </summary>
            //List<Category> categories = new List<Category>();
            //categories.Add(new Category("Rap"));
            //categories.Add(new Category("Music"));
            //categories.Add(new Category("MC"));
            //categories.Add(new Category("DJ"));
            //categories.Add(new Category("Equipment"));
            //categories.Add(new Category("Clothes"));
            //categories.Add(new Category("Other"));

            //foreach (Category c in categories)
            //{
            //    context.Categories.Add(c);
            //}
            //context.SaveChanges();

            /////<summary>
            /////Create posts
            ///// </summary>
            //List<ApplicationUser> users = context.Users.ToList();
            //List<Category> categories = context.Categories.ToList();
            //List<Post> posts = new List<Post>();
            //string description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus faucibus et sapien vitae ornare. Suspendisse sit amet suscipit lorem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus faucibus et sapien vitae ornare. Suspendisse sit amet suscipit lorem.";

            //posts.Add(new Post("First post", description, 0, "4.1", "20$", "Test URL 1", getFileBytes("\\Images\\biggie.jpg"), "image/jpeg", "https://www.youtube.com/embed/_JZom_gVfuw", users[0].Id, categories[0].Id));
            //posts.Add(new Post("Second post", description, 0, "3.9", "45$", "Test URL 2", getFileBytes("\\Images\\biggie.jpg"), "image/jpeg", "https://www.youtube.com/embed/_JZom_gVfuw", users[0].Id, categories[0].Id));
            //posts.Add(new Post("Third post", description, 0, "4.6", "30$", "Test URL 3", getFileBytes("\\Images\\biggie.jpg"), "image/jpeg", "https://www.youtube.com/embed/_JZom_gVfuw", users[0].Id, categories[0].Id));

            //foreach (Post p in posts)
            //{
            //    context.Posts.Add(p);
            //}
            //context.SaveChanges();

            ///<summary>
            ///Create comments
            /// </summary>
            //List<Post> postsWithComments = context.Posts.ToList();
            //foreach (Post p in postsWithComments)
            //{
            //    Comment c = new Comment("Test nickname", "Test comment", p.Id, p);
            //    p.Comments.Add(c);
            //}
            //context.SaveChanges();
        }

        //Gets a byte array from a file at the specified path. Used to seed images 
        private byte[] getFileBytes(string path)
        {
            FileStream fileOnDisk = new FileStream(HttpRuntime.AppDomainAppPath + path, FileMode.Open);
            byte[] fileBytes;
            using (BinaryReader br = new BinaryReader(fileOnDisk))
            {
                fileBytes = br.ReadBytes((int)fileOnDisk.Length);
            }
            return fileBytes;
        }
    }
}
